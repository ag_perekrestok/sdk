<?php

namespace PerekrestokSdk\EventListener;

use PerekrestokSdk\Http\Dto\ExceptionDto;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;

final class ApiExceptionListener implements EventSubscriberInterface
{
    private string $env;

    public function __construct(string $env)
    {
        $this->env = $env;
    }

    public static function getSubscribedEvents(): array
    {
        return [KernelEvents::EXCEPTION => 'onKernelException'];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $exceptionDto = (new ExceptionDto($exception))->getData();

        $response = new JsonResponse($this->env === 'prod' ? [$exceptionDto['message']] : $exceptionDto);

        if ($event->getThrowable() instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $event->setResponse($response);
    }
}