<?php

namespace PerekrestokSdk\Http\Dto;

class ErrorDto
{
    private array $errors;

    public function __construct(string $errorMessage)
    {
        $this->errors = [
            [
                'message' => $errorMessage
            ]
        ];
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}