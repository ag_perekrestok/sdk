<?php

namespace PerekrestokSdk\Http\Dto;

use Symfony\Component\Serializer\Annotation as Serializer;
use Throwable;

final class ExceptionDto
{
    private Throwable $exception;

    public function __construct(Throwable $exception)
    {
        $this->exception = $exception;
    }

    /**
     * @Serializer\Groups({"body"})
     */
    public function getData(): array
    {
        return [
            'exception' => get_class($this->exception),
            'message' => $this->exception->getMessage(),
            'trace' => $this->exception->getTraceAsString(),
            'file' => $this->exception->getFile(),
            'line' => $this->exception->getLine(),
        ];
    }
}