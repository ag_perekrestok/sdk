<?php

namespace PerekrestokSdk\Http\Dto;

class ItemsDto
{
    private int $total;
    private array $items;

    public function __construct(array $items)
    {
        $this->total = count($items);
        $this->items = $items;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getItems(): array
    {
        return $this->items;
    }
}