<?php

namespace PerekrestokSdk\Http\Dto;

use Symfony\Component\HttpFoundation\Request;

interface RequestDtoInterface
{
    public function __construct(Request $request);
}